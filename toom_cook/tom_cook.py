#! /usr/local/bin/python3.6
"""
Multiplication of big-digit values with Toom-Cook 3-way method
"""
import random
import sys
import traceback

import argparse

class MultiplyToomCook3:
    #D_MAX = 729  # Maximum number of digits (power of 3)
    D_MAX = 729  # Maximum number of digits (power of 3)
    #D     = 100  # Digits of computation (<= D_MAX)

    def __init__(self, input_a, input_b):
        #self.a = [input_a]
        #self.b = [input_b]
        #self.a = [random.randrange(10) for _ in range(self.D)]
        #self.b = [random.randrange(10) for _ in range(self.D)]

        """
        tmp_a = [i for i in map(int, str(input_a))]
        tmp_b = [i for i in map(int, str(input_b))]
        
        if len(tmp_a) > len(tmp_b):
            zero_pad = [0 for i in range(len(tmp_a) - len(tmp_b))]
            tmp_b = zero_pad + tmp_b
        elif len(tmp_a) < len(tmp_b):
            zero_pad = [0 for i in range(len(tmp_b) - len(tmp_a))]
            tmp_a = zero_pad + tmp_a
        print(tmp_a, tmp_b) 
        #self.a = tmp_a
        #self.b = tmp_b
        self.a = [1,2]
        self.b = [0,5]
        """
        self.a = [1,2]
        self.b = [0,1]

    def compute(self):
        """ Computation of multiplication """
        try:
            for i in range(self.D_MAX - len(self.a)):
                self.a.append(0)
            for i in range(self.D_MAX - len(self.b)):
                self.b.append(0)
            z = self.__multiply_toom_cook_3(self.a, self.b)
            z = self.__do_carry(z)
            z_value = self.__display(self.a, self.b, z)
            return z_value
        except Exception as e:
            raise
    def __multiply_normal(self, a, b):
        """ Normal multiplication
        :param  list a
        :param  list b
        :return list z
        """
        try:
            a_len, b_len = len(a), len(b)
            z = [0 for _ in range(a_len + b_len)]
            for j in range(b_len):
                for i in range(a_len):
                    z[j + i] += a[i] * b[j]
            return z
        except Exception as e:
            raise

    def __multiply_toom_cook_3(self, a, b):
        """ Toom-Cook 3-way multiplication
        :param  list a
        :param  list b
        :return list z
        """
        a_m1, a_m2, a_0, a_1, a_inf = [], [], [], [], []
        b_m1, b_m2, b_0, b_1, b_inf = [], [], [], [], []
        c_m1, c_m2, c_0, c_1, c_inf = [], [], [], [], []
        c0, c1, c2, c3, c4          = [], [], [], [], []
        try:
            t_len = len(a)
            if t_len <= 9:
                return self.__multiply_normal(a, b)
            a0 = a[:(t_len // 3)]
            a1 = a[(t_len // 3):(t_len * 2 // 3)]
            a2 = a[(t_len * 2 // 3):]
            b0 = b[:(t_len // 3)]
            b1 = b[(t_len // 3):(t_len * 2 // 3)]
            b2 = b[(t_len * 2 // 3):]
            for i in range(t_len // 3):
                a_m2.append((a2[i] << 2) - (a1[i] << 1) + a0[i])
                b_m2.append((b2[i] << 2) - (b1[i] << 1) + b0[i])
            for i in range(t_len // 3):
                a_m1.append(a2[i] - a1[i] + a0[i])
                b_m1.append(b2[i] - b1[i] + b0[i])
            a_0, b_0 = a0, b0
            for i in range(t_len // 3):
                a_1.append(a2[i] + a1[i] + a0[i])
                b_1.append(b2[i] + b1[i] + b0[i])
            a_inf, b_inf= a2, b2
            c_m2  = self.__multiply_toom_cook_3(a_m2, b_m2)
            c_m1  = self.__multiply_toom_cook_3(a_m1, b_m1)
            c_0   = self.__multiply_toom_cook_3(a_0, b_0)
            c_1   = self.__multiply_toom_cook_3(a_1, b_1)
            c_inf = self.__multiply_toom_cook_3(a_inf, b_inf)
            c4 = c_inf
            for i in range((t_len // 3) * 2):
                c  = -c_m2[i]
                c += (c_m1[i] << 1) + c_m1[i]
                c -= (c_0[i] << 1) + c_0[i]
                c += c_1[i]
                c += (c_inf[i] << 3) + (c_inf[i] << 2)
                c  = c // 6
                c3.append(c)
            for i in range((t_len // 3) * 2):
                c  = (c_m1[i] << 1) + c_m1[i]
                c -= (c_0[i] << 2) + (c_0[i] << 1)
                c += (c_1[i] << 1) + c_1[i]
                c -= (c_inf[i] << 2) + (c_inf[i] << 1)
                c  = c // 6
                c2.append(c)
            for i in range((t_len // 3) * 2):
                c  = c_m2[i]
                c -= (c_m1[i] << 2) + (c_m1[i] << 1)
                c += (c_0[i] << 1) + c_0[i]
                c += (c_1[i] << 1)
                c -= (c_inf[i] << 3) + (c_inf[i] << 2)
                c  = c // 6
                c1.append(c)
            c0 = c_0
            z = c0 + c2 + c4
            for i in range((t_len // 3) * 2):
                z[i + t_len // 3] += c1[i]
            for i in range((t_len // 3) * 2):
                z[i + t_len] += c3[i]
            return z
        except Exception as e:
            raise

    def __do_carry(self, a):
        """ Process of carrying
        :param  list a
        :return list a
        """
        cr = 0

        try:
            for i in range(len(a)):
                a[i] += cr
                cr = a[i] // 10
                a[i] -= cr * 10
            if cr != 0:
                print("[ OVERFLOW!! ] ", cr)
            return a
        except Exception as e:
            raise

    def __display(self, a, b, z):
        def convert(list):
            s = [str(i) for i in list]
            res= "".join(s)
            return res

        """ Display
        :param list a
        :param list b
        :param list z
        """
        a_len = self.D_MAX
        b_len = self.D_MAX
        z_len = self.D_MAX * 2
        try:
            while a[a_len - 1] == 0:
                if a[a_len - 1] == 0:
                    a_len -= 1
            while b[b_len - 1] == 0:
                if b[b_len - 1] == 0:
                    b_len -= 1
            while z[z_len - 1] == 0:
                if z[z_len - 1] == 0:
                    z_len -= 1
            #print("a =")
            for i in reversed(range(a_len)):
                #print(a[i], end="")
                pass
                if (a_len - i) % 10 == 0:
                    #print(" ", end="")
                    pass
                if (a_len - i) % 50 == 0:
                    #print()
                    pass
            #print()
            #print("b =")
            for i in reversed(range(b_len)):
                #print(b[i], end="")
                pass
                if (b_len - i) % 10 == 0:
                    #print(" ", end="")
                    pass
                if (b_len - i) % 50 == 0:
                    #print()
                    pass
            #print()
            #print("z =")
            z_list = []
            for i in reversed(range(z_len)):
                #print(z[i], end="")
                z_list.append(z[i])
                if (z_len - i) % 10 == 0:
                    #print(" ", end="")
                    pass
                if (z_len - i) % 50 == 0:
                    #print()
                    pass
            #print()
            z_value = convert(z_list)
            #print("z_list", int(z_value))
            return z_value
        except Exception as e:
            raise



if __name__ == '__main__':
    
    parser = argparse.ArgumentParser(description='nnRSA_Toom_COOK\n')
    parser.add_argument('--input_a')
    parser.add_argument('--input_b')
    args=parser.parse_args() 
    
    args.input_a = int(args.input_a)
    args.input_b = int(args.input_b)
    
    #working correctly
    obj = MultiplyToomCook3(args.input_a, args.input_b)
    z_value = int(obj.compute())
    print("z = ", z_value)

    #exponential
    """ 
    tmp_value = args.input_a
    for i in range(args.input_b):
        obj = MultiplyToomCook3(tmp_value, args.input_a)
        z_value = int (obj.compute())
        tmp_value = z_value 

    print("tmp_value = ", tmp_value)
    """


