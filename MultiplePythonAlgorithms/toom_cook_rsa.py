#! /usr/local/bin/python3.6
"""
Multiplication of big-digit values with Toom-Cook 3-way method
"""
import random
import sys
import traceback
import timeit
import argparse

add = 0
mult = 0
div = 0
shift = 0
compare = 0
mod = 0

add_bits = 0
mult_bits = 0
div_bits = 0
shift_bits = 0
compare_bits = 0
mod_bits = 0

class MultiplyToomCook3Counters:
    D_MAX = 729  # Maximum number of digits (power of 3)

    def __init__(self, input_a, input_b):
        tmp_a = [str(i) for i in map(int, str(input_a))]
        tmp_b = [str(i) for i in map(int, str(input_b))]
      
        # split into many pieces with 729 ranges
        def list_chunk(lst,n):
            new_list = []
            for i in range(0, len(lst), n):
                new_list.append(int("".join(lst[i:i+n])))
            return new_list
        
        self.a = list_chunk(tmp_a,728)[::-1]
        self.b = list_chunk(tmp_b,728)[::-1]

    def compute(self):
        """ Computation of multiplication """
        try:
            for i in range(self.D_MAX - len(self.a)):
                self.a.append(0)
            for i in range(self.D_MAX - len(self.b)):
                self.b.append(0)
            z = self.__multiply_toom_cook_3(self.a, self.b)
            z = self.__do_carry(z)
            z_value = self.__display(self.a, self.b, z)
            return z_value
        except Exception as e:
            raise

    def __multiply_normal(self, a, b):
        """ Normal multiplication
        :param  list a
        :param  list b
        :return list z
        """

        global add, mult, add_bits, mult_bits

        try:
            a_len, b_len = len(a), len(b)
            z = [0 for _ in range(a_len + b_len)]
            add += 1
            add_bits += a_len.bit_length() + b_len.bit_length()
            for j in range(b_len):
                for i in range(a_len):
                    z[j + i] += a[i] * b[j]
                    add += 1
                    add_bits += j.bit_length() + i.bit_length()
                    mult += 1
                    mult_bits += a[i].bit_length() + b[j].bit_length()
            return z
        except Exception as e:
            raise

    def __multiply_toom_cook_3(self, a, b):
        """ Toom-Cook 3-way multiplication
        :param  list a
        :param  list b
        :return list z
        """

        global div, mult, shift, add, compare, div_bits, mult_bits, shift_bits, add_bits, compare_bits

        a_m1, a_m2, a_0, a_1, a_inf = [], [], [], [], []
        b_m1, b_m2, b_0, b_1, b_inf = [], [], [], [], []
        c_m1, c_m2, c_0, c_1, c_inf = [], [], [], [], []
        c0, c1, c2, c3, c4          = [], [], [], [], []
        try:
            t_len = len(a)
            compare += 1
            compare_bits += t_len.bit_length() + 4
            if t_len <= 9:
                return self.__multiply_normal(a, b)
            
            a0 = a[:(t_len // 3)]
            a1 = a[(t_len // 3):(t_len * 2 // 3)]
            a2 = a[(t_len * 2 // 3):]
            b0 = b[:(t_len // 3)]
            b1 = b[(t_len // 3):(t_len * 2 // 3)]
            b2 = b[(t_len * 2 // 3):]
            div += 8
            div_bits += 4*t_len.bit_length() + 4*(2*t_len).bit_length() + 8*2
            mult += 4
            mult_bits += t_len.bit_length() + 2

            div += 1
            div_bits += t_len.bit_length() + 2
            for i in range(t_len // 3):
                shift += 4
                shift_bits += a2[i].bit_length() + a1[i].bit_length() + b2[i].bit_length() + b1[i].bit_length()
                add += 4
                add_bits += a2[i].bit_length() + a1[i].bit_length() + b2[i].bit_length() + b1[i].bit_length() + a0[i].bit_length() + b0[i].bit_length() + 6 # 2 + 2 + 1 + 1 = 6
                a_m2.append((a2[i] << 2) - (a1[i] << 1) + a0[i])
                b_m2.append((b2[i] << 2) - (b1[i] << 1) + b0[i])
            div += 1
            div_bits += t_len.bit_length() + 2
            for i in range(t_len // 3):
                add += 4
                add_bits += a2[i].bit_length() + a1[i].bit_length() + b2[i].bit_length() + b1[i].bit_length() + a0[i].bit_length() + b0[i].bit_length()
                a_m1.append(a2[i] - a1[i] + a0[i])
                b_m1.append(b2[i] - b1[i] + b0[i])
            a_0, b_0 = a0, b0
            div += 1
            div_bits += t_len.bit_length() + 2
            for i in range(t_len // 3):
                add += 4
                add_bits += a2[i].bit_length() + a1[i].bit_length() + b2[i].bit_length() + b1[i].bit_length() + a0[i].bit_length() + b0[i].bit_length()
                a_1.append(a2[i] + a1[i] + a0[i])
                b_1.append(b2[i] + b1[i] + b0[i])
            a_inf, b_inf= a2, b2
            c_m2  = self.__multiply_toom_cook_3(a_m2, b_m2)
            c_m1  = self.__multiply_toom_cook_3(a_m1, b_m1)
            c_0   = self.__multiply_toom_cook_3(a_0, b_0)
            c_1   = self.__multiply_toom_cook_3(a_1, b_1)
            c_inf = self.__multiply_toom_cook_3(a_inf, b_inf)
            c4 = c_inf
            div += 1
            div_bits += t_len.bit_length() + 2
            mult += 1
            mult_bits += (t_len // 3).bit_length() + 2
            for i in range((t_len // 3) * 2):
                c  = -c_m2[i]
                shift += 1
                add += 2
                shift_bits += c_m1[i].bit_length() + 1
                add_bits += c.bit_length() + 2*c_m1[i].bit_length() + 1
                c += (c_m1[i] << 1) + c_m1[i]
                shift += 1
                add += 2
                shift_bits += c_0[i].bit_length() + 1
                add_bits += c.bit_length() + 2*c_0[i].bit_length() + 1
                c -= (c_0[i] << 1) + c_0[i]
                add += 1
                add_bits += c.bit_length() + c_1[i].bit_length()
                c += c_1[i]
                shift += 2
                add += 2
                shift_bits += 2*c_inf[i].bit_length() + 5
                add_bits += c.bit_length() + 2*c_inf[i].bit_length() + 5
                c += (c_inf[i] << 3) + (c_inf[i] << 2)
                div += 1
                div_bits += c.bit_length() + 3
                c  = c // 6
                c3.append(c)
            div += 1
            div_bits += t_len.bit_length() + 2
            mult += 1
            mult_bits += (t_len // 3).bit_length() + 2
            for i in range((t_len // 3) * 2):
                shift += 1
                add += 2
                shift_bits += c_m1[i].bit_length() + 1
                add_bits += c.bit_length() + 2*c_m1[i].bit_length() + 1
                c  = (c_m1[i] << 1) + c_m1[i]
                shift += 2
                add += 2
                shift_bits += 2*c_0[i].bit_length() + 3
                add_bits += c.bit_length() + 2*c_0[i].bit_length() + 3
                c -= (c_0[i] << 2) + (c_0[i] << 1)
                shift += 1
                add += 2
                shift_bits += c_1[i].bit_length() + 1
                add_bits += c.bit_length() + 2*c_1[i].bit_length() + 1
                c += (c_1[i] << 1) + c_1[i]
                shift += 2
                add += 2
                shift_bits += 2*c_inf[i].bit_length() + 2 + 1
                add_bits += c.bit_length() + 2*c_inf[i].bit_length() + 3
                c -= (c_inf[i] << 2) + (c_inf[i] << 1)
                div += 1
                div_bits += c.bit_length() + 3
                c  = c // 6
                c2.append(c)
            div += 1
            div_bits += t_len.bit_length() + 2
            mult += 1
            mult_bits += (t_len // 3).bit_length() + 2
            for i in range((t_len // 3) * 2):
                c  = c_m2[i]
                shift += 2
                add += 2
                shift_bits += 2*c_m1[i].bit_length() + 3
                add_bits += c.bit_length() + 2*c_m1[i].bit_length() + 1
                c -= (c_m1[i] << 2) + (c_m1[i] << 1)
                shift += 1
                add += 2
                shift_bits += c_0[i].bit_length() + 1
                add_bits += c.bit_length() + 2*c_0[i].bit_length() + 1
                c += (c_0[i] << 1) + c_0[i]
                shift += 1
                add += 1
                shift_bits += c_1[i].bit_length() + 1
                add_bits += c.bit_length() + c_1[i].bit_length() + 1
                c += (c_1[i] << 1)
                shift += 2
                add += 2
                shift_bits += 2*c_inf[i].bit_length() + 2 + 2
                add_bits += c.bit_length() + 2*c_inf[i].bit_length() + 5
                c -= (c_inf[i] << 3) + (c_inf[i] << 2)
                div += 1
                div_bits += c.bit_length() + 3
                c  = c // 6
                c1.append(c)
            c0 = c_0
            add += 2
            #add_bits += c0.bit_length() + c2.bit_length() + c4.bit_length()
            #AttributeError: 'list' object has no attribute 'bit_length'
            add_bits += 3*t_len
            z = c0 + c2 + c4
            div += 1
            div_bits += t_len.bit_length() + 2
            mult += 1
            mult_bits += (t_len // 3).bit_length() + 2
            for i in range((t_len // 3) * 2):
                add += 2
                div += 1
                add_bits += i.bit_length() + (t_len // 3).bit_length() + z[i + t_len // 3].bit_length() + c1[i].bit_length()
                div_bits += t_len.bit_length() + 2
                z[i + t_len // 3] += c1[i]
            div += 1
            div_bits += t_len.bit_length() + 2
            mult += 1
            mult_bits += (t_len // 3).bit_length() + 2
            for i in range((t_len // 3) * 2):
                add += 2
                add_bits += i.bit_length() + t_len.bit_length() + z[i + t_len].bit_length() + c3[i].bit_length()
                z[i + t_len] += c3[i]
            return z
        except Exception as e:
            raise

    def __do_carry(self, a):
        """ Process of carrying
        :param  list a
        :return list a
        """

        global add, div, mult, compare, add_bits, div_bits, mult_bits, compare_bits

        cr = 0

        try:
            for i in range(len(a)):
                add += 1
                add_bits += a[i].bit_length() + cr.bit_length()
                a[i] += cr
                div += 1
                div_bits += a[i].bit_length() + 4
                cr = a[i] // 10
                add += 1
                mult += 1
                add_bits += a[i].bit_length() + (cr * 10).bit_length()
                mult_bits += cr.bit_length() + 4
                a[i] -= cr * 10
            compare += 1
            compare_bits += cr.bit_length()
            if cr != 0:
                pass
            return a
        except Exception as e:
            raise

    def __display(self, a, b, z):
        """ Display
        :param list a
        :param list b
        :param list z
        """
        def convert(list):
            s = [str(i) for i in list]
            res= "".join(s)
            return res
        
        global add, mod, compare, add_bits, mod_bits, compare_bits
        
        a_len = self.D_MAX
        b_len = self.D_MAX
        z_len = self.D_MAX * 2
        try:
            add += 1
            add_bits += a_len.bit_length() + 1
            while a[a_len - 1] == 0:
                add += 1
                add_bits += a_len.bit_length() + 1
                compare += 1
                compare_bits += a[a_len - 1].bit_length()
                if a[a_len - 1] == 0:
                    add += 1
                    add_bits += a_len.bit_length() + 1
                    a_len -= 1
            add += 1
            add_bits += b_len.bit_length() + 1
            while b[b_len - 1] == 0:
                add += 1
                add_bits += b_len.bit_length() + 1
                compare += 1
                compare_bits += b[b_len - 1].bit_length()
                if b[b_len - 1] == 0:
                    add += 1
                    add_bits += b_len.bit_length() + 1
                    b_len -= 1
            add += 1
            add_bits += z_len.bit_length() + 1
            while z[z_len - 1] == 0:
                add += 1
                add_bits += z_len.bit_length() + 1
                compare += 1
                compare_bits += z[z_len - 1].bit_length()
                if z[z_len - 1] == 0:
                    add += 1
                    add_bits += z_len.bit_length() + 1
                    z_len -= 1
            for i in reversed(range(a_len)):
                add += 1
                compare += 1
                mod += 1
                add_bits += a_len.bit_length() + i.bit_length()
                compare_bits += ((a_len - i) % 10).bit_length()
                mod_bits += (a_len - i).bit_length() + 4
                if (a_len - i) % 10 == 0:
                    pass
                add += 1
                compare += 1
                mod += 1
                add_bits += a_len.bit_length() + i.bit_length()
                compare_bits += ((a_len - i) % 50).bit_length()
                mod_bits += (a_len - i).bit_length() + 7
                if (a_len - i) % 50 == 0:
                    pass
            for i in reversed(range(b_len)):
                add += 1
                compare += 1
                mod += 1
                add_bits += b_len.bit_length() + i.bit_length()
                compare_bits += ((b_len - i) % 10).bit_length()
                mod_bits += (b_len - i).bit_length() + 4
                if (b_len - i) % 10 == 0:
                    pass
                add += 1
                compare += 1
                mod += 1
                add_bits += b_len.bit_length() + i.bit_length()
                compare_bits += ((b_len - i) % 50).bit_length()
                mod_bits += (b_len - i).bit_length() + 7
                if (b_len - i) % 50 == 0:
                    pass
            z_list = []
            for i in reversed(range(z_len)):
                z_list.append(z[i])
                add += 1
                compare += 1
                mod += 1
                add_bits += z_len.bit_length() + i.bit_length()
                compare_bits += ((z_len - i) % 10).bit_length()
                mod_bits += (z_len - i).bit_length() + 4
                if (z_len - i) % 10 == 0:
                    pass
                add += 1
                compare += 1
                mod += 1
                add_bits += z_len.bit_length() + i.bit_length()
                compare_bits += ((z_len - i) % 50).bit_length()
                mod_bits += (z_len - i).bit_length() + 7
                if (z_len - i) % 50 == 0:
                    pass
            z_value = convert(z_list)
            return z_value
        except Exception as e:
            raise

# copy of pow mod that calls karatsuba instead of mul_mod
def pow_mod_toom_cook(a, n, m): # m(message), e(65537), n(modulo) 
    
    global mod, shift, mod_bits, shift_bits
    
    r = 1

    while n > 0:
        if n & 1:
            obj = MultiplyToomCook3(r, a)
            mod += 1
            mod_bits += (int(obj.compute())).bit_length() + m.bit_length()
            r = int(obj.compute()) % m
        
        obj = MultiplyToomCook3(a, a)
        mod += 1
        mod_bits += (int(obj.compute())).bit_length() + m.bit_length()
        a = int(obj.compute()) % m
        shift += 1
        shift_bits += n.bit_length() + 1
        n >>= 1

    return r    

# ATTENTION
"""
Below code is identical except it doesn't have counters
Timing is affected by adding counters so we have to run two sets of the same code
One for counting time and one for counting operations/bits
"""
# ATTENTION

class MultiplyToomCook3:
    D_MAX = 729  # Maximum number of digits (power of 3)

    def __init__(self, input_a, input_b):
        tmp_a = [str(i) for i in map(int, str(input_a))]
        tmp_b = [str(i) for i in map(int, str(input_b))]
      
        # split into many pieces with 729 ranges
        def list_chunk(lst,n):
            new_list = []
            for i in range(0, len(lst), n):
                new_list.append(int("".join(lst[i:i+n])))
            return new_list
        
        self.a = list_chunk(tmp_a,728)[::-1]
        self.b = list_chunk(tmp_b,728)[::-1]

    def compute(self):
        """ Computation of multiplication """
        try:
            for i in range(self.D_MAX - len(self.a)):
                self.a.append(0)
            for i in range(self.D_MAX - len(self.b)):
                self.b.append(0)
            z = self.__multiply_toom_cook_3(self.a, self.b)
            z = self.__do_carry(z)
            z_value = self.__display(self.a, self.b, z)
            return z_value
        except Exception as e:
            raise

    def __multiply_normal(self, a, b):
        """ Normal multiplication
        :param  list a
        :param  list b
        :return list z
        """
        try:
            a_len, b_len = len(a), len(b)
            z = [0 for _ in range(a_len + b_len)]
            for j in range(b_len):
                for i in range(a_len):
                    z[j + i] += a[i] * b[j]
            return z
        except Exception as e:
            raise

    def __multiply_toom_cook_3(self, a, b):
        """ Toom-Cook 3-way multiplication
        :param  list a
        :param  list b
        :return list z
        """
        a_m1, a_m2, a_0, a_1, a_inf = [], [], [], [], []
        b_m1, b_m2, b_0, b_1, b_inf = [], [], [], [], []
        c_m1, c_m2, c_0, c_1, c_inf = [], [], [], [], []
        c0, c1, c2, c3, c4          = [], [], [], [], []
        try:
            t_len = len(a)
            if t_len <= 9:
                return self.__multiply_normal(a, b)
            a0 = a[:(t_len // 3)]
            a1 = a[(t_len // 3):(t_len * 2 // 3)]
            a2 = a[(t_len * 2 // 3):]
            b0 = b[:(t_len // 3)]
            b1 = b[(t_len // 3):(t_len * 2 // 3)]
            b2 = b[(t_len * 2 // 3):]
            for i in range(t_len // 3):
                a_m2.append((a2[i] << 2) - (a1[i] << 1) + a0[i])
                b_m2.append((b2[i] << 2) - (b1[i] << 1) + b0[i])
            for i in range(t_len // 3):
                a_m1.append(a2[i] - a1[i] + a0[i])
                b_m1.append(b2[i] - b1[i] + b0[i])
            a_0, b_0 = a0, b0
            for i in range(t_len // 3):
                a_1.append(a2[i] + a1[i] + a0[i])
                b_1.append(b2[i] + b1[i] + b0[i])
            a_inf, b_inf= a2, b2
            c_m2  = self.__multiply_toom_cook_3(a_m2, b_m2)
            c_m1  = self.__multiply_toom_cook_3(a_m1, b_m1)
            c_0   = self.__multiply_toom_cook_3(a_0, b_0)
            c_1   = self.__multiply_toom_cook_3(a_1, b_1)
            c_inf = self.__multiply_toom_cook_3(a_inf, b_inf)
            c4 = c_inf
            for i in range((t_len // 3) * 2):
                c  = -c_m2[i]
                c += (c_m1[i] << 1) + c_m1[i]
                c -= (c_0[i] << 1) + c_0[i]
                c += c_1[i]
                c += (c_inf[i] << 3) + (c_inf[i] << 2)
                c  = c // 6
                c3.append(c)
            for i in range((t_len // 3) * 2):
                c  = (c_m1[i] << 1) + c_m1[i]
                c -= (c_0[i] << 2) + (c_0[i] << 1)
                c += (c_1[i] << 1) + c_1[i]
                c -= (c_inf[i] << 2) + (c_inf[i] << 1)
                c  = c // 6
                c2.append(c)
            for i in range((t_len // 3) * 2):
                c  = c_m2[i]
                c -= (c_m1[i] << 2) + (c_m1[i] << 1)
                c += (c_0[i] << 1) + c_0[i]
                c += (c_1[i] << 1)
                c -= (c_inf[i] << 3) + (c_inf[i] << 2)
                c  = c // 6
                c1.append(c)
            c0 = c_0
            z = c0 + c2 + c4
            for i in range((t_len // 3) * 2):
                z[i + t_len // 3] += c1[i]
            for i in range((t_len // 3) * 2):
                z[i + t_len] += c3[i]
            return z
        except Exception as e:
            raise

    def __do_carry(self, a):
        """ Process of carrying
        :param  list a
        :return list a
        """
        cr = 0

        try:
            for i in range(len(a)):
                a[i] += cr
                cr = a[i] // 10
                a[i] -= cr * 10
            if cr != 0:
                pass
            return a
        except Exception as e:
            raise

    def __display(self, a, b, z):
        """ Display
        :param list a
        :param list b
        :param list z
        """
        def convert(list):
            s = [str(i) for i in list]
            res= "".join(s)
            return res
        
        a_len = self.D_MAX
        b_len = self.D_MAX
        z_len = self.D_MAX * 2
        try:
            while a[a_len - 1] == 0:
                if a[a_len - 1] == 0:
                    a_len -= 1
            while b[b_len - 1] == 0:
                if b[b_len - 1] == 0:
                    b_len -= 1
            while z[z_len - 1] == 0:
                if z[z_len - 1] == 0:
                    z_len -= 1
            for i in reversed(range(a_len)):
                if (a_len - i) % 10 == 0:
                    pass
                if (a_len - i) % 50 == 0:
                    pass
            for i in reversed(range(b_len)):
                if (b_len - i) % 10 == 0:
                    pass
                if (b_len - i) % 50 == 0:
                    pass
            z_list = []
            for i in reversed(range(z_len)):
                z_list.append(z[i])
                if (z_len - i) % 10 == 0:
                    pass
                if (z_len - i) % 50 == 0:
                    pass
            z_value = convert(z_list)
            return z_value
        except Exception as e:
            raise

# copy of pow mod that calls karatsuba instead of mul_mod
def pow_mod_toom_cook_counters(a, n, m): # m(message), e(65537), n(modulo) 
    r = 1

    while n > 0:
        if n & 1:
            obj = MultiplyToomCook3Counters(r, a)
            r = int(obj.compute()) % m
        
        obj = MultiplyToomCook3Counters(a, a)
        a = int(obj.compute()) % m
        n >>= 1

    return r

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='\nRSA_Toom_COOK\n')
    parser.add_argument('--message')
    parser.add_argument('--e_value')
    parser.add_argument('--modulus')
    args=parser.parse_args() 
    
    args.message = int(args.message)
    args.e_value = int(args.e_value, 16) # base 16 (OpenSSL gives exponent and modulus as hexadecimal values)
    args.modulus = int(args.modulus, 16)
    
    start_enc = timeit.default_timer()
    encryption_result = pow_mod_toom_cook(args.message, 65537, args.modulus)
    enc_time = timeit.default_timer() - start_enc
    
    encryption_result_counters = pow_mod_toom_cook_counters(args.message, 65537, args.modulus)
    if encryption_result != encryption_result_counters:
        print("incorrect! encryption result != encryption result counters, ", encryption_result, " != ", encryption_result_counters, "\n")

    enc_add = add
    enc_mult = mult
    enc_div = div
    enc_shift = shift
    enc_compare = compare
    enc_mod = mod

    enc_add_bits = add_bits
    enc_mult_bits = mult_bits
    enc_div_bits = div_bits
    enc_shift_bits = shift_bits
    enc_compare_bits = compare_bits
    enc_mod_bits = mod_bits
    
    start_dec = timeit.default_timer()
    decryption_result = pow_mod_toom_cook(encryption_result, args.e_value, args.modulus)
    dec_time = timeit.default_timer() - start_dec
    total_time = enc_time + dec_time

    decryption_result_counters = pow_mod_toom_cook_counters(encryption_result, args.e_value, args.modulus)
    if decryption_result != args.message or decryption_result_counters != args.message:
        print("incorrect! decryption result != decryption result counters != original message, ", decryption_result, " != ", decryption_result_counters, " != ", args.message, "\n")

    print(enc_time, ", ", dec_time, ", ", total_time, ", ", \
    enc_add, ", ", enc_mult, ", ", enc_div, ", ", enc_shift, ", ", enc_compare, ", ", enc_mod, ", ", \
    enc_add_bits, ", ", enc_mult_bits, ", ", enc_div_bits, ", ", enc_shift_bits, ", ", enc_compare_bits, ", ", enc_mod_bits, ", ", \
    add, ", ", mult, ", ", div, ", ", shift, ", ", compare, ", ", mod, ", ", \
    add_bits, ", ", mult_bits, ", ", div_bits, ", ", shift_bits, ", ", compare_bits, ", ", mod_bits)
