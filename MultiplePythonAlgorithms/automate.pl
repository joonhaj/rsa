#! /usr/bin/perl

$loop = 50;

`rm rsa_output.txt`;
`touch rsa_output.txt`;
`python2 print_csv_headers.py >> rsa_output.txt`;

`rm rsa_output_karatsuba.txt`;
`touch rsa_output_karatsuba.txt`;
`python2 print_csv_headers_karatsuba.py >> rsa_output_karatsuba.txt`;

`rm rsa_output_toom_cook.txt`;
`touch rsa_output_toom_cook.txt`;
`python2 print_csv_headers_toom_cook.py >> rsa_output_toom_cook.txt`;

`rm rsa_output_fft.txt`;
`touch rsa_output_fft.txt`;
`python2 print_csv_headers_fft.py >> rsa_output_fft.txt`;

for($a = 0; $a < $loop; $a = $a + 1){
    # create new key
    `openssl genrsa -out example.key`;
    $modulus_long = `openssl rsa -noout -modulus -in example.key`; #extract modulus
    $modulus = substr($modulus_long, 8); #remove "modulus="
    $modulus =~ tr/\n\t //d; # remove new lines, tabs, and spaces

    $private_key_long = `openssl pkey -in example.key -text -noout`;
    $private_key = substr($private_key_long, 960, 855); #extract private key
    $private_key =~ tr/:\n\tpr //d; #remove colons, new lines, tabs, and spaces (and pr if we overflow to "prime1")

    # run python code, args = modulus (n), message (m), private key (d)
    #$message = 12345;
    $message = int(rand(100000)) + 1; # the message to be encrypted is a random number between 1 and 100,000
    print "message = ";
    print $message;
    `python3 rsa.py $modulus $message $private_key >> rsa_output.txt`;
    `python2 rsa_karatsuba.py $modulus $message $private_key >> rsa_output_karatsuba.txt`;
    `python3 toom_cook_rsa.py --message $message --e_value $private_key --modulus $modulus >> rsa_output_toom_cook.txt`;
    `python3 fft_rsa.py --message $message --e_value $private_key --modulus $modulus >> rsa_output_fft.txt`;
}
