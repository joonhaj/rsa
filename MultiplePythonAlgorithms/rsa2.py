#!/usr/bin/env python

import timeit
import sys
from bitstring import BitArray
# I wanted to use pyRAPL to measure power but it's not importing correctly
# (I tried using git clone in this directory and then importing it)
# it can also be added using "pip install pyRAPL" but that doesn't work on CAEN
# https://github.com/powerapi-ng/pyRAPL
#import os
#sys.path.append(os.path.abspath('pyRAPL/pyRAPL'))
#import pyRAPL

#pyRAPL.setup()

#@pyRAPL.measure
# rsa encryption function (multiple algorithms are used, timed, and have operations counted)
def rsaEnc():
    # read variables in from command line and convert to ints
    e=65537 # e = encryption exponent
    nString=sys.argv[1] # n = modulo
    mString=sys.argv[2] # m = message
    n=int(nString, 16)
    m=int(mString, 10)
    #print "m, e, n",m,e,n

    # time methods using timeit, call algorithms
    start_pow_mod = timeit.default_timer()
    result1 = pow_mod(m, e, n)
    end_pow_mod = timeit.default_timer()
    result2 = expm(m, e, n)
    end_expm = timeit.default_timer()
    result3 = m ** e % n # no algorithm
    end_no_algorithm = timeit.default_timer()
    result4 = pow_mod_moduloMult(m, e, n)
    end_moduloMult = timeit.default_timer()
    #result5 = pow_mod_karatsuba(m, e, n)
    #end_karatsuba = timeit.default_timer()
    result6 = pow_mod_booths(m, e, n)
    end_booths = timeit.default_timer()

    # print times
    global enc_pow_mod, enc_binary_exp, enc_no_alg, enc_modulo_mult, enc_karatsuba, enc_booths
    enc_pow_mod = end_pow_mod - start_pow_mod
    enc_binary_exp = end_expm - end_pow_mod
    enc_no_alg = end_no_algorithm - end_expm
    enc_modulo_mult = end_moduloMult - end_no_algorithm
    enc_karatsuba = 0#end_karatsuba - end_moduloMult
    enc_booths = end_booths - end_moduloMult
    # these are printed csv style at the end now
    # print "encryption with pow mod time = ", end_pow_mod - start_pow_mod
    # print "encryption with binary exponentiation time = ", end_expm - end_pow_mod
    # print "encryption with no algorithm time = ", end_no_algorithm - end_expm
    # print "encryption with modulo mult time = ", end_moduloMult - end_no_algorithm

    # check that results match
    if result1 != result2 or result3 != result4 or result1 != result3 or result1 != result6:
        print("error in rsaEnc, result1 = ", result1, ", result2 = ", result2, ", result3 = ", result3, ", result4 = ", result4, "result6 = ", result6)
    # commented out because - if error didn't print we can assume it worked and not bother printing this every time
    # else:
    #     print "rsaEnc success, result 1 = ", result1, " = result2 = ", result2, ", result3 = ", result3, ", result4 = ", result4
    return result1

# rsa decryption function (multiple algorithms are used, timed, and have operations counted)
def rsaDec(c):
    # read variables in from command line and convert to ints
    nString=sys.argv[1] # n = modulo
    mString=sys.argv[2] # m = original message
    dString=sys.argv[3] # d = decryption exponent
    n=int(nString, 16)
    original_message=int(mString, 10)
    d=int(dString, 16)
    #print "n, d, c" #,n,d,c

    # time methods using timeit, call algorithms
    start_pow_mod = timeit.default_timer()
    result1 = pow_mod(c, d, n)
    end_pow_mod = timeit.default_timer()
    result2 = expm(c, d, n)
    end_expm = timeit.default_timer()
    #this ran for ~20 minutes and then I killed it
    result3 = c ** d % n # no algorithm
    end_no_algorithm = timeit.default_timer()
    result3 = pow_mod_moduloMult(c, d, n)
    end_moduloMult = timeit.default_timer()
    #this ran for 344 seconds the first time I tried it, I think we should just use the encryption info to save time everytime we run the script
    result4 = pow_mod_karatsuba(c, d, n)
    end_karatsuba = timeit.default_timer()

    # print times
    global dec_pow_mod, dec_binary_exp, dec_modulo_mult, dec_karatsuba
    dec_pow_mod = end_pow_mod - start_pow_mod
    dec_binary_exp = end_expm - end_pow_mod
    dec_modulo_mult = end_moduloMult - end_expm
    dec_karatsuba = end_karatsuba - end_moduloMult
    # these are printed csv style at the end now
    print( "decryption with pow mod time = ", end_pow_mod - start_pow_mod)
    print( "decryption with binary exponentiation time = ", end_expm - end_pow_mod)
    print( "decryption with no algorithm time = ", end_no_algorithm - end_expm)
    print( "decryption with modulo mult time = ", end_moduloMult - end_expm)

    # check that results match
    if result1 != result2 or result1 != result3:# (this causes problems every other iteration and I don't know why) or result1 != original_message:
        print("error in rsaDec, original message = ", original_message, "result1 = ", result1, ", result2 = ", result2, ", result3 = ", result3)
    # commented out because - if error didn't print we can assume it worked and not bother printing this every time
    # else:
    print( "rsaDec success, result 1 = ", result1, " = result2 = ", result2, ", result3 = ", result3)
    return result1

# booth's algorithm
# http://philosophyforprogrammers.blogspot.com/2011/05/booths-multiplication-algorithm-in.html
# https://www.quora.com/How-can-I-get-the-bit-length-of-a-number-in-python
shift_counter_booths = 0
add_counter_booths = 0
modulo_counter_booths = 0
def booths(m, r):
    global shift_counter_booths, add_counter_booths
    
    # how should we count this function call?
    add_counter_booths += 2
    x = m.bit_length() + 1
    y = r.bit_length() + 1
    
    add_counter_booths += 2
    totalLength = x + y + 1
    
    # how should we count this function call?
    add_counter_booths += 2
    shift_counter_booths += 1
    mA = BitArray(int = m, length = totalLength)
    A = mA << (y+1)
    S = BitArray(int = -m, length = totalLength)  << (y+1)
    P = BitArray(int = r, length = y)
    P.prepend(BitArray(int = 0, length = x))
    P = P << 1
    
    add_counter_booths += 1
    for i in range(1,y+1):
        if P[-2:] == '0b01':
            add_counter_booths += 1
            P = BitArray(int = P.int + A.int, length = totalLength)
        elif P[-2:] == '0b10':
            add_counter_booths += 1
            P = BitArray(int = P.int + S.int, length = totalLength)
        shift_counter_booths += 1
        P = arith_shift_right(P, 1)
    shift_counter_booths += 1
    P = arith_shift_right(P, 1)
    return P.int

def arith_shift_right(x, amt):
    l = x.len
    # shifting counted in booths function
    x = BitArray(int = (x.int >> amt), length = l)
    return x

# copy of pow mod that calls booths instead of mul_mod
def pow_mod_booths(a, n, m):
    global modulo_counter_booths, shift_counter_booths
    
    r = 1

    while n > 0:
        if n & 1:
            modulo_counter_booths += 1
            r = booths(r, a) % m
        modulo_counter_booths += 1
        shift_counter_booths += 1
        a = booths(a, a) % m
        n >>= 1
    
    return r

# karatsuba multiplication
# https://pythonandr.com/2015/10/13/karatsuba-multiplication-algorithm-python-code/ 
# this breaks maximum recursion depth in python3
# mult_counter_karatsuba = 0
# modulo_counter_karatsuba = 0
# add_counter_karatsuba = 0
# shift_counter_karatsuba = 0
# exp_counter_karatsuba = 0
# div_counter_karatsuba = 0
def karatsuba(x, y):
     global mult_counter_karatsuba, modulo_counter_karatsuba, add_counter_karatsuba, shift_counter_karatsuba, exp_counter_karatsuba, div_counter_karatsuba
    
     if (len(str(x)) == 1) or len(str(y)) == 1:
         mult_counter_karatsuba += 1
         return x*y
     else:
         n = max(len(str(x)), len(str(y)))
         shift_counter_karatsuba += 1
         nby2 = n >> 1 
         nby2 = n / 2

         exp_counter_karatsuba += 1
         modulo_counter_karatsuba += 2
         div_counter_karatsuba += 2
         ten_exp_nby2 = 10**(nby2)
         a = x / ten_exp_nby2
         b = x % ten_exp_nby2
         c = y / ten_exp_nby2
         d = y % ten_exp_nby2

         ac = karatsuba(a, c)
         bd = karatsuba(b, d)
         add_counter_karatsuba += 2
         ad_plus_bc = karatsuba(a+b, c+d) - ac - bd

         # writing n as 2 * nby2 takes care of both even and odd n
         exp_counter_karatsuba += 1
         mult_counter_karatsuba += 2
         add_counter_karatsuba += 2
         prod = ac * 10**(2*nby2) + (ad_plus_bc * ten_exp_nby2) + bd

         return prod

# copy of pow mod that calls karatsuban instead of mul_mod
def pow_mod_karatsuba(a, n, m):
     original_a = a
     original_n = n
     original_m = m
     global shift_counter_karatsuba, modulo_counter_karatsuba
    
     r = 1

     while n > 0:
         if n & 1:
             modulo_counter_karatsuba += 1
             r = karatsuba(r, a) % m
         modulo_counter_karatsuba += 1
         a = karatsuba(a, a) % m
         shift_counter_karatsuba += 1
         n >>= 1
    
#     # check that this code actually does what it's supposed to
#     if r != (original_a ** original_n % original_m):
#     print("error in pow_mod_karatsuba, r = ", r, " != a ^ n % m = ", original_a, " ^ ", original_n, " % ", original_m, " = ", original_a ** original_n % original_m)
#     print( "error in pow_mod_karatsuba, r = ", r, " != a ^ n % m = ", original_a, " ^ ", original_n, " % ", original_m, " = ", original_a ** original_n % original_m)
#     return r

# binary exponentiation github
# https://github.com/jnyryan/rsa-encryption/tree/9ca2a734f3a0b7e0c6fd7aaf382c7b8368191811  
mult_counter_expm = 0
modulo_counter_expm = 0
def expm(a, k, n):
    global mult_counter_expm, modulo_counter_expm

    r = 1
    bits = list(bin(k)[2:])
    for bit in bits:
        mult_counter_expm += 1
        modulo_counter_expm += 1
        r = (r * r) % n
        if int(bit) == 1:
            mult_counter_expm += 1
            modulo_counter_expm += 1
            r = (r * a) % n
    return r

# geeks for geeks
# https://www.geeksforgeeks.org/multiply-large-integers-under-large-modulo/ 
# I think this code does the exact same thing as the stack overflow code but I wanted to test it and see, the time is actually a bit better for this version
mult_counter_modulo_mult = 0
modulo_counter_modulo_mult = 0
add_counter_modulo_mult = 0
shift_counter_modulo_mult = 0
def moduloMultiplication(a, b, mod):
    # original_a = a
    # original_b = b
    # original_m = mod
    global mult_counter_modulo_mult, modulo_counter_modulo_mult, add_counter_modulo_mult, shift_counter_modulo_mult

    res = 0 # initialize result

    modulo_counter_modulo_mult += 1
    a %= mod # update a if it is >= mod

    while b:
        # if b is odd, add a and result
        if b & 1:
            add_counter_modulo_mult += 1
            modulo_counter_modulo_mult += 1
            res = (res + a) % mod       
        
        shift_counter_modulo_mult += 2
        modulo_counter_modulo_mult += 1
        a = (a << 1) % mod # a = (a * 2) % mod
        b >>= 1 # b = b / 2

    # check that this code actually does what it's supposed to
    #if res != (original_a * original_b % original_m):
    #    print "error in moduloMultiplication, r = ", res, ", a * b % m = ", original_a, " * ", original_b, " % ", original_m, " = ", original_a * original_b % original_m

    return res

# copy of pow mod that calls moduloMultiplication instead of mul_mod
def pow_mod_moduloMult(a, n, m):
    # original_a = a
    # original_n = n
    # original_m = m
    global shift_counter_modulo_mult
    
    r = 1

    while n > 0:
        if n & 1:
            r = moduloMultiplication(r, a, m)
        a = moduloMultiplication(a, a, m)
        shift_counter_modulo_mult += 1
        n >>= 1
    
    # check that this code actually does what it's supposed to
    #if r != (original_a ** original_n % original_m):
    #    print "error in pow_mod, r = ", r, " != a ^ n % m = ", original_a, " ^ ", original_n, " % ", original_m, " = ", original_a ** original_n % original_m
    return r

# stack overflow
# https://stackoverflow.com/questions/20111827/various-questions-about-rsa-encryption/20114154#20114154
# we can hopefully have different versions of this mul_mod function that uses different multiplication algorithms?
mult_counter_pow_mod = 0
modulo_counter_pow_mod = 0
add_counter_pow_mod = 0
shift_counter_pow_mod = 0
def mul_mod(a, b, m):
    # original_a = a
    # original_b = b
    # original_m = m
    global mult_counter_pow_mod, modulo_counter_pow_mod, add_counter_pow_mod, shift_counter_pow_mod

    if m == 0:
        mult_counter_pow_mod += 1
        return a * b

    r = 0
    while a > 0:
        if a & 1:
            add_counter_pow_mod += 1
            r += b
            if r > m:
                modulo_counter_pow_mod += 1
                r %= m
        shift_counter_pow_mod += 2
        a >>= 1
        b <<= 1
        if b > m:
            modulo_counter_pow_mod += 1
            b %= m

    # check that this code actually does what it's supposed to
    #if r != (original_a * original_b % original_m):
    #    print "error in mul_mod, r = ", r, ", a * b % m = ", original_a, " * ", original_b, " % ", original_m, " = ", original_a * original_b % original_m
    return r

# stack overflow
# https://stackoverflow.com/questions/20111827/various-questions-about-rsa-encryption/20114154#20114154
def pow_mod(a, n, m):
    # original_a = a
    # original_n = n
    # original_m = m
    global shift_counter_pow_mod
    
    r = 1

    while n > 0:
        if n & 1:
            r = mul_mod(r, a, m)
        a = mul_mod(a, a, m)
        shift_counter_pow_mod += 1
        n >>= 1
    
    # check that this code actually does what it's supposed to
    #if r != (original_a ** original_n % original_m):
    #    print "error in pow_mod, r = ", r, " != a ^ n % m = ", original_a, " ^ ", original_n, " % ", original_m, " = ", original_a ** original_n % original_m
    return r

#####################################################################
# Tests

if __name__ == "__main__":
    #print "RSA Python Running . . ."
    
    # mult_counter = 0
    # modulo_counter = 0

    cipher = rsaEnc()
    enc_mult_counter_pow_mod = mult_counter_pow_mod
    enc_modulo_counter_pow_mod = modulo_counter_pow_mod
    enc_add_counter_pow_mod = add_counter_pow_mod
    enc_shift_counter_pow_mod = shift_counter_pow_mod
    enc_mult_counter_expm = mult_counter_expm
    enc_modulo_counter_expm = modulo_counter_expm
    enc_mult_counter_modulo_mult = mult_counter_modulo_mult
    enc_modulo_counter_modulo_mult = modulo_counter_modulo_mult
    enc_add_counter_modulo_mult = add_counter_modulo_mult
    enc_shift_counter_modulo_mult = shift_counter_modulo_mult
    #print "cipher: ",cipher

    message = rsaDec(cipher)
    #print "message:",message	
    
    #print "mult_counter", mult_counter
    #print "modulo_counter", modulo_counter

    #print "Done.\n"
    # prints timing and operand count results csv style (so they can be copied into excel and turned into graphs)
    print(enc_pow_mod, ", ", enc_binary_exp, ", ", enc_no_alg, ", ", enc_modulo_mult, ", ", enc_booths, ", ", \
    dec_pow_mod, ", ", dec_binary_exp, ", ", dec_modulo_mult, ", ", \
    enc_pow_mod + dec_pow_mod, ", ", enc_binary_exp + dec_binary_exp, ", ", enc_modulo_mult + dec_modulo_mult, ", ", \
    enc_mult_counter_pow_mod, ", ", enc_modulo_counter_pow_mod, ", ", enc_add_counter_pow_mod, ", ", enc_shift_counter_pow_mod, ", ", \
    enc_mult_counter_expm, ", ", enc_modulo_counter_expm, ", ", \
    enc_mult_counter_modulo_mult, ", ", enc_modulo_counter_modulo_mult, ", ", enc_add_counter_modulo_mult, ", ", enc_shift_counter_modulo_mult, ", ", \
    modulo_counter_booths, ", ", add_counter_booths, ", ", shift_counter_booths, ", ", \
    mult_counter_pow_mod, ", ", modulo_counter_pow_mod, ", ", add_counter_pow_mod, ", ", shift_counter_pow_mod, ", ", \
    mult_counter_expm, ", ", modulo_counter_expm, ", ", \
    mult_counter_modulo_mult, ", ", modulo_counter_modulo_mult, ", ", add_counter_modulo_mult, ", ", shift_counter_modulo_mult)
