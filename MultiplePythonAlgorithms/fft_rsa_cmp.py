#! /usr/local/bin/python3.6
from cmath import exp
from math import pi
import argparse
import numpy as np

import timeit

# Various Information Globals
mul_counter = 0
mod_counter = 0
add_counter = 0
sft_counter = 0
div_counter = 0
cmp_counter = 0

mul_bit_counter = 0
mod_bit_counter = 0
add_bit_counter = 0
sft_bit_counter = 0
div_bit_counter = 0
cmp_bit_counter = 0

class Calculation:
    def __init__(self, n, k = 1):
        self.k = k
        self.n = n

    def __pow__(self, other):
        global cmp_counter, cmp_bit_counter
        cmp_counter += 1
        cmp_bit_counter += other.bit_length()
        if type(other) is int:
            n = Calculation(self.n, self.k * other)
            return n

    def __eq__(self, other):
        global cmp_counter, cmp_bit_counter
        cmp_counter += 1
        cmp_bit_counter += other.bit_length()
        if other == 1:
            cmp_counter += 1
            cmp_bit_counter += self.n.bit_length() + self.k.bit_length()
            return abs(self.n) == abs(self.k)

    def __mul__(self, other):
        global mul_counter, mul_bit_counter, div_counter, mul_bit_counter
        mul_counter += 4 # 2 * 1j * pi * self.k * other
        mul_bit_counter += 5 + self.k.bit_length()
        div_counter += 1
        mul_bit_counter += self.n.bit_length()
        return exp(2*1j*pi*self.k/self.n)*other

    def __repr__(self):
        global add_counter, add_bit_counter
        add_counter += 1
        add_bit_counter += self.n.bit_length() + self.k.bit_length()
        return str(self.n) +  str(self.k)

    @property
    def th(self):
        return abs(self.n // self.k)


def FFT(A, omega):
    global cmp_counter, cmp_bit_counter, mul_counter, mul_bit_counter, add_counter, add_bit_counter
    cmp_counter += 1
    cmp_bit_counter += 1
    if omega == 1:
        return [sum(A)]
    o2 = omega**2
    C1 = FFT(A[0::2], o2)
    C2 = FFT(A[1::2], o2)
    C3 = [None]*omega.th
    for i in range(omega.th//2):
        add_counter += 2
        add_bit_counter += (C1[i] + i * C2[i]).bit_length()
        mul_counter += 2
        mul_bit_counter += 2 * (i * C2[i]).bit_length() 
        C3[i] = C1[i] + omega**i * C2[i]
        C3[i+omega.th//2] = C1[i] - omega**i * C2[i]
    return C3

def FPM(A,B):
    global cmp_counter, cmp_bit_counter, mod_counter, sft_counter, mod_bit_counter, sft_bit_counter, mul_counter, mul_bit_counter, div_counter, div_bit_counter, add_counter, add_bit_counter
    sft_counter += 1
    sft_bit_counter += (len(A)+len(B)-2).bit_length()
    n = 1<<(len(A)+len(B)-2).bit_length()
    o = Calculation(n)
    AT = FFT(A, o)
    BT = FFT(B, o)

    C = [AT[i]*BT[i] for i in range(n)]

    mul_counter += n
    mul_bit_counter += [C[i].bit_length() for i in range(n)]


    D = [round((a/n).real) for a in FFT(C, o ** -1)]

    div_counter += [for a in FFT(C, o ** -1)]
    div_bit_counter += [round((a/n).real).bit_length() for a in FFT(C, o ** - 1)]


    while (len(A) + len(B) -1) < len(D):
        cmp_counter += 1
        cmp_bit_counter = (len(A) + len(B) - 1).bit_length() + len(D).bit_length()
        add_counter += 2
        add_bit_counter += (len(A) + len(B) - 1).bit_length()
        del D[-1]

    return D

def power(a,b): # not used
    global cmp_counter, cmp_bit_counter, mod_counter, mod_bit_counter, mul_counter, mul_bit_counter, sft_bit_counter, sft_counter
    result = 1
    while b > 0:
        cmp_counter += 2
        cmp_bit_counter += b.bit_length() + (b % 2).bit_length()
        mod_counter += 1
        mod_bit_counter += 1
        if b % 2 != 0:
            mul_counter += 1
            mul_bit_counter += (result * a).bit_length()
            result = result * a
        sft_counter += 1
        sft_bit_counter += b.bit_length()
        b >>= 1
        mul_counter += 1
        mul_bit_counter += (a*a).bit_length()
        a = a * a

    return result

# there is a way to improve the performance
def multiplication(a,b):
    global add_counter, add_bit_counter
    input_a = [i for i in map(int, str(a))] # set message 12345
    input_b = [i for i in map(int, str(b))] # set message 12345

    value = FPM(input_a, input_b)
    
    total = 0 #sum
    for i in range(len(value)):
        add_counter += 1
        add_bit_counter += len(value[i]).bit_length()
        total += value[i] * power(10, len(value)-i-1)

    return total

# fft exponentiaion multiplication with modulus
def fft_calculation(a, n, m): # m(message), e(65537), n(modulo)
    #global shift_counter_karatsuba, modulo_counter_karatsuba
    global cmp_counter, cmp_bit_counter, mod_counter, sft_counter, mod_bit_counter, sft_bit_counter
    r = 1
    cmp_counter += 1
    cmp_bit_counter += n.bit_length()
    while n > 0: #65537
        cmp_counter += 1
        cmp_bit_counter += n.bit_length()
        if n & 1:
            mod_counter += 1
            mod_bit_counter += (r*a).bit_length() + m.bit_length()
            r = multiplication(r, a) % m
        mod_counter += 1
        mod_bit_counter += (r*a).bit_length() + m.bit_length()
        a = multiplication(a, a) % m
        sft_counter += 1
        sft_bit_counter += n.bit_length()
        n >>= 1
    return r


if __name__ == "__main__":

    # Parser set-up
    parser = argparse.ArgumentParser(description='\nRSA_fft\n')

    # Add Arguments to Parser
    parser.add_argument('--message')
    parser.add_argument('--e_value')
    parser.add_argument('--modulus')
    args = parser.parse_args()

    args.message = int(args.message)
    args.e_value = int(args.e_value, 16)
    args.modulus = int(args.modulus, 16)

    # Timing Calculations for Encryption
    start = timeit.default_timer()
    encrypted_data = fft_calculation(args.message, 65537, args.modulus)	
    end = timeit.default_timer()
    
    # Encryption Time
    enc_time = end - start


    # Encryption Information
    enc_mul_counter = mul_counter
    enc_mod_counter = mod_counter
    enc_add_counter = add_counter
    enc_sft_counter = sft_counter
    enc_div_counter = div_counter
    enc_cmp_counter = cmp_counter

    enc_mul_bit_counter = mul_bit_counter
    enc_mod_bit_counter = mod_bit_counter
    enc_add_bit_counter = add_bit_counter
    enc_sft_bit_counter = sft_bit_counter
    enc_div_bit_counter = div_bit_counter
    enc_cmp_bit_counter = cmp_bit_counter

    # Timing Calculations for Decryption
    dec_start = timeit.default_timer()
    decrypted_data = fft_calculation(encrypted_data, args.e_value, args.modulus)	
    dec_end = timeit.default_timer()

    # Decryption Time 
    dec_time = dec_end - dec_start

    # Print Statements
    print (enc_time, ", ", dec_time, ", ", enc_time + dec_time, ", ", \
    enc_mul_counter, ", ", enc_mod_counter, ", ", enc_add_counter, ", ", enc_sft_counter, ", ",
    enc_div_counter, ", ", enc_cmp_counter, ", ", enc_mul_bit_counter, ", ", enc_mod_bit_counter, ", ", 
    enc_add_bit_counter, ", ", enc_sft_bit_counter, ", ", enc_div_bit_counter, ", ", enc_cmp_bit_counter, ", ", 
    mul_counter, ", ", mod_counter, ", ", add_counter, ", ", sft_counter, ", ",
    div_counter, ", ", cmp_counter, ", ", mul_bit_counter, ", ", mod_bit_counter, ", ", 
    add_bit_counter, ", ", sft_bit_counter, ", ", div_bit_counter, ", ", cmp_bit_counter)
    





