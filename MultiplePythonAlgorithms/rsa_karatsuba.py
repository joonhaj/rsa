#!/usr/bin/env python

import timeit
import sys

# rsa encryption function - calls pow_mod with karatsuba algorithm
def rsaEnc():
    # read variables in from command line and convert to ints
    e=65537 # e = encryption exponent
    nString=sys.argv[1] # n = modulo
    mString=sys.argv[2] # m = message
    n=int(nString, 16)
    m=int(mString, 10)

    # call karatsuba encryption code and time it (without counters)
    start = timeit.default_timer()
    result = pow_mod_karatsuba(m, e, n)
    end = timeit.default_timer()

    # call karatsuba with counters (don't need to time this)
    pow_mod_karatsuba_with_counters(m, e, n)

    # save encryption time
    global enc_time
    enc_time = end - start

    return result

# rsa decryption function - calls pow_mod with karatsuba algorithm
def rsaDec(c):
    # read variables in from command line and convert to ints
    nString=sys.argv[1] # n = modulo
    mString=sys.argv[2] # m = original message
    dString=sys.argv[3] # d = decryption exponent
    n=int(nString, 16)
    original_message=int(mString, 10)
    d=int(dString, 16)

    # call karatsuba encryption code and time it (without counters)
    start = timeit.default_timer()
    result1 = pow_mod_karatsuba(c, d, n)
    end = timeit.default_timer()

    # call karatsuba with counters (don't need to time this)
    result2 = pow_mod_karatsuba_with_counters(c, d, n)

    # save decryption time
    global dec_time
    dec_time = end - start

    # check that results match
    if result1 != original_message or result2 != original_message:
        print("error in rsaDec, original message = ", original_message, "result1 = ", result1, ", result2 = ", result2)
    
    return result1

# karatsuba multiplication
# https://pythonandr.com/2015/10/13/karatsuba-multiplication-algorithm-python-code/ 
# this breaks maximum recursion depth in python3 - solution, make new rsa.py file with just karatsuba and run that with python2, combine results in excel
mult_counter_karatsuba = 0
modulo_counter_karatsuba = 0
add_counter_karatsuba = 0
shift_counter_karatsuba = 0
exp_counter_karatsuba = 0
div_counter_karatsuba = 0
cmp_counter_karatsuba = 0

mult_counter_karatsuba_bits = 0
modulo_counter_karatsuba_bits = 0
add_counter_karatsuba_bits = 0
shift_counter_karatsuba_bits = 0
exp_counter_karatsuba_bits = 0
div_counter_karatsuba_bits = 0
cmp_counter_karatsuba_bits = 0

def karatsuba_with_counters(x, y):
    global mult_counter_karatsuba, modulo_counter_karatsuba, add_counter_karatsuba, shift_counter_karatsuba, exp_counter_karatsuba, div_counter_karatsuba, cmp_counter_karatsuba
    global mult_counter_karatsuba_bits, modulo_counter_karatsuba_bits, add_counter_karatsuba_bits, shift_counter_karatsuba_bits, exp_counter_karatsuba_bits, div_counter_karatsuba_bits, cmp_counter_karatsuba_bits
    
    # how should we count the len function calls?
    cmp_counter_karatsuba += 1
    cmp_counter_karatsuba_bits += x.bit_length() + y.bit_length() + 2
    if (len(str(x)) == 1) or len(str(y)) == 1:
        mult_counter_karatsuba += 1
        mult_counter_karatsuba_bits += x.bit_length() + y.bit_length()
        return x*y
    else:
        n = max(len(str(x)), len(str(y)))
        shift_counter_karatsuba += 1
        shift_counter_karatsuba_bits += n.bit_length()
        nby2 = n >> 1 # nby2 = n / 2

        exp_counter_karatsuba += 1
        modulo_counter_karatsuba += 2
        div_counter_karatsuba += 2
        ten = 10
        exp_counter_karatsuba_bits += ten.bit_length() + nby2.bit_length()
        ten_exp_nby2 = 10**(nby2)
        modulo_counter_karatsuba_bits += x.bit_length() + y.bit_length() + 2*ten_exp_nby2.bit_length()
        div_counter_karatsuba_bits += x.bit_length() + y.bit_length() + 2*ten_exp_nby2.bit_length()
        a = x / ten_exp_nby2
        b = x % ten_exp_nby2
        c = y / ten_exp_nby2
        d = y % ten_exp_nby2

        ac = karatsuba_with_counters(a, c)
        bd = karatsuba_with_counters(b, d)
        add_counter_karatsuba += 2
        add_counter_karatsuba_bits += ((a+b)*(c+d)).bit_length() + ac.bit_length() + bd.bit_length()
        ad_plus_bc = karatsuba(a+b, c+d) - ac - bd

        # writing n as 2 * nby2 takes care of both even and odd n
        exp_counter_karatsuba += 1
        mult_counter_karatsuba += 2
        add_counter_karatsuba += 2
        exp_counter_karatsuba_bits += ten.bit_length() + (2*nby2).bit_length()
        mult_counter_karatsuba_bits += ac.bit_length() + (ac * 10**(2*nby2)).bit_length() + ad_plus_bc.bit_length() + ten_exp_nby2.bit_length()
        add_counter_karatsuba_bits += (ac * 10**(2*nby2)).bit_length() + (ad_plus_bc * ten_exp_nby2).bit_length() + bd.bit_length()
        prod = ac * 10**(2*nby2) + (ad_plus_bc * ten_exp_nby2) + bd

        return prod

# copy of pow mod that calls karatsuba instead of mul_mod
def pow_mod_karatsuba_with_counters(a, n, m):
    global shift_counter_karatsuba, modulo_counter_karatsuba, cmp_counter_karatsuba
    global shift_counter_karatsuba_bits, modulo_counter_karatsuba_bits, cmp_counter_karatsuba_bits
    
    r = 1

    while n > 0:
        cmp_counter_karatsuba += 1
        cmp_counter_karatsuba_bits += n.bit_length() + 1
        if n & 1:
            modulo_counter_karatsuba += 1
            modulo_counter_karatsuba_bits += (r*a).bit_length() + m.bit_length()
            r = karatsuba_with_counters(r, a) % m
        modulo_counter_karatsuba += 1
        modulo_counter_karatsuba_bits += (a*a).bit_length() + m.bit_length()
        a = karatsuba_with_counters(a, a) % m
        shift_counter_karatsuba += 1
        shift_counter_karatsuba_bits += n.bit_length()
        n >>= 1

    return r

# without counters
def karatsuba(x, y):    
    if (len(str(x)) == 1) or len(str(y)) == 1:
        return x*y
    else:
        n = max(len(str(x)), len(str(y)))
        nby2 = n >> 1 # nby2 = n / 2

        ten_exp_nby2 = 10**(nby2)
        a = x / ten_exp_nby2
        b = x % ten_exp_nby2
        c = y / ten_exp_nby2
        d = y % ten_exp_nby2

        ac = karatsuba(a, c)
        bd = karatsuba(b, d)
        ad_plus_bc = karatsuba(a+b, c+d) - ac - bd

        # writing n as 2 * nby2 takes care of both even and odd n
        prod = ac * 10**(2*nby2) + (ad_plus_bc * ten_exp_nby2) + bd

        return prod

# without counters
def pow_mod_karatsuba(a, n, m):   
    r = 1

    while n > 0:
        if n & 1:
            r = karatsuba(r, a) % m
        a = karatsuba(a, a) % m
        n >>= 1

    return r

#####################################################################
# Tests

if __name__ == "__main__":
    cipher = rsaEnc()

    enc_mult_counter_karatsuba = mult_counter_karatsuba
    enc_modulo_counter_karatsuba = modulo_counter_karatsuba
    enc_add_counter_karatsuba = add_counter_karatsuba
    enc_shift_counter_karatsuba = shift_counter_karatsuba
    enc_exp_counter_karatsuba = exp_counter_karatsuba
    enc_div_counter_karatsuba = div_counter_karatsuba
    enc_cmp_counter_karatsuba = cmp_counter_karatsuba

    enc_mult_counter_karatsuba_bits = mult_counter_karatsuba_bits
    enc_modulo_counter_karatsuba_bits = modulo_counter_karatsuba_bits
    enc_add_counter_karatsuba_bits = add_counter_karatsuba_bits
    enc_shift_counter_karatsuba_bits = shift_counter_karatsuba_bits
    enc_exp_counter_karatsuba_bits = exp_counter_karatsuba_bits
    enc_div_counter_karatsuba_bits = div_counter_karatsuba_bits
    enc_cmp_counter_karatsuba_bits = cmp_counter_karatsuba_bits

    rsaDec(cipher)

    # prints timing and operand count results csv style (so they can be copied into excel and turned into graphs)
    print enc_time, ", ", dec_time, ", ", enc_time + dec_time, ", ", \
    enc_mult_counter_karatsuba, ", ", enc_modulo_counter_karatsuba, ", ", enc_add_counter_karatsuba, ", ", enc_shift_counter_karatsuba, ", ", enc_exp_counter_karatsuba, ", ", enc_div_counter_karatsuba, ", ", enc_cmp_counter_karatsuba, ", ", \
    enc_mult_counter_karatsuba_bits, ", ", enc_modulo_counter_karatsuba_bits, ", ", enc_add_counter_karatsuba_bits, ", ", enc_shift_counter_karatsuba_bits, ", ", enc_exp_counter_karatsuba_bits, ", ", enc_div_counter_karatsuba_bits, ", ", enc_cmp_counter_karatsuba_bits, ", ", \
    mult_counter_karatsuba, ", ", modulo_counter_karatsuba, ", ", add_counter_karatsuba, ", ", shift_counter_karatsuba, ", ", exp_counter_karatsuba, ", ", div_counter_karatsuba, ", ", cmp_counter_karatsuba, ", ", \
    mult_counter_karatsuba_bits, ", ", modulo_counter_karatsuba_bits, ", ", add_counter_karatsuba_bits, ", ", shift_counter_karatsuba_bits, ", ", exp_counter_karatsuba_bits, ", ", div_counter_karatsuba_bits, ", ", cmp_counter_karatsuba_bits
