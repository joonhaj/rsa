#!/usr/bin/env python

import timeit
import sys
from bitstring import BitArray

# rsa encryption function (multiple algorithms are used, timed, and have operations counted)
exponent_counter_no_algorithm_bits = 0
modulo_counter_no_algorithm_bits = 0
def rsaEnc():
    # read variables in from command line and convert to ints
    e=65537 # e = encryption exponent
    nString=sys.argv[1] # n = modulo
    mString=sys.argv[2] # m = message
    n=int(nString, 16)
    m=int(mString, 10)

    # time methods using timeit, call algorithms without counters
    start_expm = timeit.default_timer()
    result1 = expm(m, e, n)
    end_expm = timeit.default_timer()
    result2 = m ** e % n # no algorithm
    end_no_algorithm = timeit.default_timer()
    result3 = pow_mod_moduloMult(m, e, n)
    end_moduloMult = timeit.default_timer()
    result4 = pow_mod_booths(m, e, n)
    end_booths = timeit.default_timer()

    # call algorithms with counters (don't need to time these)
    result5 = expm_with_counters(m, e, n)
    result6 = pow_mod_moduloMult_with_counters(m, e, n)
    result7 = pow_mod_booths_with_counters(m, e, n)

    # calculate bit counters for no algorithm version
    global exponent_counter_no_algorithm_bits, modulo_counter_no_algorithm_bits
    exponent_counter_no_algorithm_bits += m.bit_length() + e.bit_length()
    modulo_counter_no_algorithm_bits += (m ** e).bit_length() + n.bit_length()

    # save times as global variables
    global enc_binary_exp, enc_no_alg, enc_modulo_mult, enc_booths
    enc_binary_exp = end_expm - start_expm
    enc_no_alg = end_no_algorithm - end_expm
    enc_modulo_mult = end_moduloMult - end_no_algorithm
    enc_booths = end_booths - end_moduloMult

    # check that results match
    if result1 != result2 or result1 != result3 or result1 != result4 or result1 != result5 or result1 != result6 or result1 != result7:
        print("error in rsaEnc, result1 = ", result1, ", result2 = ", result2, ", result3 = ", result3, ", result4 = ", result4, "result5 = ", result5, "result6 = ", result6, "result7 = ", result7)

    return result1

# rsa decryption function (multiple algorithms are used, timed, and have operations counted)
def rsaDec(c):
    # read variables in from command line and convert to ints
    nString=sys.argv[1] # n = modulo
    mString=sys.argv[2] # m = original message
    dString=sys.argv[3] # d = decryption exponent
    n=int(nString, 16)
    original_message=int(mString, 10)
    d=int(dString, 16)

    # time methods using timeit, call algorithms
    start_expm = timeit.default_timer()
    result1 = expm(c, d, n)
    end_expm = timeit.default_timer()
    #this ran for ~20 minutes and then I killed it
    #result2 = c ** d % n # no algorithm
    #end_no_algorithm = timeit.default_timer()
    result3 = pow_mod_moduloMult(c, d, n)
    end_moduloMult = timeit.default_timer()
    # this ran for ~1 hour and then I killed it
    # result4 = pow_mod_booths(c, d, n)
    # end_booths = timeit.default_timer()

    # call algorithms with counters (don't need to time these)
    result5 = expm_with_counters(c, d, n)
    result6 = pow_mod_moduloMult_with_counters(c, d, n)
    # result7 = pow_mod_booths_with_counters(c, d, n)

    # save times as global variables
    global dec_binary_exp, dec_modulo_mult#, dec_booths
    dec_binary_exp = end_expm - start_expm
    dec_modulo_mult = end_moduloMult - end_expm
    # dec_booths = end_booths - end_moduloMult

    # check that results match
    if result1 != result3 or result1 != result5 or result1 != result6 or result1 != original_message:
        print("error in rsaDec, original message = ", original_message, "result1 = ", result1, ", result3 = ", result3, "result5 = ", result5, "result6 = ", result6)
    
    return result1

# booth's algorithm
# http://philosophyforprogrammers.blogspot.com/2011/05/booths-multiplication-algorithm-in.html
# https://www.quora.com/How-can-I-get-the-bit-length-of-a-number-in-python
shift_counter_booths = 0
add_counter_booths = 0
modulo_counter_booths = 0
comparison_counter_booths = 0
shift_counter_booths_bits = 0
add_counter_booths_bits = 0
modulo_counter_booths_bits = 0
comparison_counter_booths_bits = 0
def booths_with_counters(m, r):
    global shift_counter_booths, add_counter_booths, comparison_counter_booths
    global shift_counter_booths_bits, add_counter_booths_bits, comparison_counter_booths_bits
    
    # how should we count these bit_length function call? (in the actual code not in the counter code)
    add_counter_booths += 2
    add_counter_booths_bits += m.bit_length().bit_length() + r.bit_length().bit_length() + 2
    x = m.bit_length() + 1
    y = r.bit_length() + 1
    
    add_counter_booths += 2
    add_counter_booths_bits += x.bit_length() + y.bit_length() + 1
    totalLength = x + y + 1
    
    # how should we count these bitarray function calls?
    add_counter_booths += 2
    shift_counter_booths += 2
    add_counter_booths_bits += 2*y.bit_length() + 2
    mA = BitArray(int = m, length = totalLength)
    A = mA << (y+1)
    S = BitArray(int = -m, length = totalLength)  << (y+1)
    P = BitArray(int = r, length = y)
    P.prepend(BitArray(int = 0, length = x))
    # shift_counter_booths_bits += mA.bit_length() + (y+1).bit_length() + P.bit_length() + 1, this didn't work because bit_length doesn't work on bitarrays, fixed below 
    shift_counter_booths_bits += totalLength + (y+1).bit_length() + x + 1 # mA's length = totalLength, P's length = x
    P = P << 1
    
    add_counter_booths += 1
    add_counter_booths_bits += y.bit_length() + 1
    for i in range(1,y+1):
        comparison_counter_booths += 2
        comparison_counter_booths_bits += 8
        if P[-2:] == '0b01':
            add_counter_booths += 1
            #add_counter_booths_bits += P.bit_length() + A.bit_length()
            add_counter_booths_bits += x + totalLength + y + 1 # P's length = x, A's length = mA's length (totalLength) + y + 1
            P = BitArray(int = P.int + A.int, length = totalLength)
        elif P[-2:] == '0b10':
            add_counter_booths += 1
            #add_counter_booths_bits += P.bit_length() + S.bit_length()
            add_counter_booths_bits += x + totalLength + y + 1 
            P = BitArray(int = P.int + S.int, length = totalLength)
        shift_counter_booths += 1
        #shift_counter_booths_bits += P.bit_length() + 1
        shift_counter_booths_bits += x + 1
        P = arith_shift_right(P, 1)
    shift_counter_booths += 1
    #shift_counter_booths_bits += P.bit_length() + 1
    shift_counter_booths_bits += x + 1
    P = arith_shift_right(P, 1)
    return P.int

# copy of pow mod that calls booths
# https://stackoverflow.com/questions/20111827/various-questions-about-rsa-encryption/20114154#20114154
# https://www.geeksforgeeks.org/modular-exponentiation-power-in-modular-arithmetic/
def pow_mod_booths_with_counters(a, n, m):
    global modulo_counter_booths, shift_counter_booths, comparison_counter_booths
    global modulo_counter_booths_bits, shift_counter_booths_bits, comparison_counter_booths_bits

    r = 1

    while n > 0:
        comparison_counter_booths += 1
        comparison_counter_booths_bits += n.bit_length() + 1
        if n & 1:
            modulo_counter_booths += 1
            modulo_counter_booths_bits += (r*a).bit_length() + m.bit_length()
            r = booths_with_counters(r, a) % m
        modulo_counter_booths += 1
        shift_counter_booths += 1
        modulo_counter_booths_bits += (a*a).bit_length() + m.bit_length()
        shift_counter_booths_bits += n.bit_length()
        a = booths_with_counters(a, a) % m
        n >>= 1
    
    return r

# wthout counters
def booths(m, r):
    x = m.bit_length() + 1
    y = r.bit_length() + 1

    totalLength = x + y + 1
    
    mA = BitArray(int = m, length = totalLength)
    A = mA << (y+1)
    S = BitArray(int = -m, length = totalLength)  << (y+1)
    P = BitArray(int = r, length = y)
    P.prepend(BitArray(int = 0, length = x))
    P = P << 1
    
    for i in range(1,y+1):
        if P[-2:] == '0b01':
            P = BitArray(int = P.int + A.int, length = totalLength)
        elif P[-2:] == '0b10':
            P = BitArray(int = P.int + S.int, length = totalLength)
        P = arith_shift_right(P, 1)
    P = arith_shift_right(P, 1)
    return P.int

# one copy, doesn't need counters, shift counted in booths function
def arith_shift_right(x, amt):
    l = x.len
    x = BitArray(int = (x.int >> amt), length = l)
    return x

# without counters
def pow_mod_booths(a, n, m):
    r = 1

    while n > 0:
        if n & 1:
            r = booths(r, a) % m
        a = booths(a, a) % m
        n >>= 1
    
    return r


# binary exponentiation github
# https://github.com/jnyryan/rsa-encryption/tree/9ca2a734f3a0b7e0c6fd7aaf382c7b8368191811  
mult_counter_expm = 0
modulo_counter_expm = 0
comparison_counter_expm = 0
mult_counter_expm_bits = 0
modulo_counter_expm_bits = 0
comparison_counter_expm_bits = 0
def expm_with_counters(a, k, n):
    global mult_counter_expm, modulo_counter_expm, comparison_counter_expm, mult_counter_expm_bits, modulo_counter_expm_bits, comparison_counter_expm_bits

    r = 1
    bits = list(bin(k)[2:])
    for bit in bits:
        mult_counter_expm += 1
        modulo_counter_expm += 1
        mult_counter_expm_bits += 2*r.bit_length()
        modulo_counter_expm_bits += (r*r).bit_length() + n.bit_length()
        r = (r * r) % n
        comparison_counter_expm += 1
        comparison_counter_expm_bits += 2 # bit is 1 bit compared to 1 is 1 bit
        if int(bit) == 1:
            mult_counter_expm += 1
            modulo_counter_expm += 1
            mult_counter_expm_bits += r.bit_length() + a.bit_length()
            modulo_counter_expm_bits += (r*a).bit_length() + n.bit_length()
            r = (r * a) % n
    return r

def expm(a, k, n):
    r = 1
    bits = list(bin(k)[2:])
    for bit in bits:
        r = (r * r) % n
        if int(bit) == 1:
            r = (r * a) % n
    return r

# geeks for geeks
# https://www.geeksforgeeks.org/multiply-large-integers-under-large-modulo/ 
# I think this code does the exact same thing as the stack overflow code but I wanted to test it and see, the time is actually a bit better for this version
modulo_counter_modulo_mult = 0
add_counter_modulo_mult = 0
shift_counter_modulo_mult = 0
comparison_counter_modulo_mult = 0
modulo_counter_modulo_mult_bits = 0
add_counter_modulo_mult_bits = 0
shift_counter_modulo_mult_bits = 0
comparison_counter_modulo_mult_bits = 0
def moduloMultiplication_with_counters(a, b, mod):
    # original_a = a
    # original_b = b
    # original_m = mod
    global modulo_counter_modulo_mult, add_counter_modulo_mult, shift_counter_modulo_mult, comparison_counter_modulo_mult
    global modulo_counter_modulo_mult_bits, add_counter_modulo_mult_bits, shift_counter_modulo_mult_bits, comparison_counter_modulo_mult_bits

    res = 0 # initialize result

    modulo_counter_modulo_mult += 1
    modulo_counter_modulo_mult_bits += a.bit_length() + mod.bit_length()
    a %= mod # update a if it is >= mod

    while b:
        # if b is odd, add a and result
        comparison_counter_modulo_mult += 1
        comparison_counter_modulo_mult_bits += b.bit_length() + 1
        if b & 1:
            add_counter_modulo_mult += 1
            modulo_counter_modulo_mult += 1
            add_counter_modulo_mult_bits += res.bit_length() + a.bit_length()
            modulo_counter_modulo_mult_bits += (res + a).bit_length() + mod.bit_length()
            res = (res + a) % mod       
        
        shift_counter_modulo_mult += 2
        modulo_counter_modulo_mult += 1
        shift_counter_modulo_mult_bits += a.bit_length() + b.bit_length()
        modulo_counter_modulo_mult_bits += a.bit_length() + 1 + mod.bit_length()
        a = (a << 1) % mod # a = (a * 2) % mod
        b >>= 1 # b = b / 2

    # check that this code actually does what it's supposed to
    #if res != (original_a * original_b % original_m):
    #    print "error in moduloMultiplication, r = ", res, ", a * b % m = ", original_a, " * ", original_b, " % ", original_m, " = ", original_a * original_b % original_m

    return res

# copy of pow mod that calls moduloMultiplication
# https://stackoverflow.com/questions/20111827/various-questions-about-rsa-encryption/20114154#20114154
# https://www.geeksforgeeks.org/modular-exponentiation-power-in-modular-arithmetic/
def pow_mod_moduloMult_with_counters(a, n, m):
    # original_a = a
    # original_n = n
    # original_m = m
    global shift_counter_modulo_mult, shift_counter_modulo_mult_bits, comparison_counter_modulo_mult, comparison_counter_modulo_mult_bits
    
    r = 1

    while n > 0:
        comparison_counter_modulo_mult += 1
        comparison_counter_modulo_mult_bits += n.bit_length() + 1
        if n & 1:
            r = moduloMultiplication_with_counters(r, a, m)
        a = moduloMultiplication_with_counters(a, a, m)
        shift_counter_modulo_mult += 1
        shift_counter_modulo_mult_bits += n.bit_length()
        n >>= 1
    
    # check that this code actually does what it's supposed to
    #if r != (original_a ** original_n % original_m):
    #    print "error in pow_mod, r = ", r, " != a ^ n % m = ", original_a, " ^ ", original_n, " % ", original_m, " = ", original_a ** original_n % original_m
    return r

# without counters version
def moduloMultiplication(a, b, mod):
    res = 0 # initialize result

    a %= mod # update a if it is >= mod

    while b:
        if b & 1:
            res = (res + a) % mod       
        a = (a << 1) % mod # a = (a * 2) % mod
        b >>= 1 # b = b / 2

    return res

# without counters version
def pow_mod_moduloMult(a, n, m):
    r = 1

    while n > 0:
        if n & 1:
            r = moduloMultiplication(r, a, m)
        a = moduloMultiplication(a, a, m)
        n >>= 1

    return r

#####################################################################
# Tests

if __name__ == "__main__":
    
    cipher = rsaEnc()
    
    # find counters for just encryption for algorithms that are going to perform decryption
    enc_mult_counter_expm = mult_counter_expm
    enc_modulo_counter_expm = modulo_counter_expm
    enc_comparison_counter_expm = comparison_counter_expm
    enc_mult_counter_expm_bits = mult_counter_expm_bits
    enc_modulo_counter_expm_bits = modulo_counter_expm_bits
    enc_comparison_counter_expm_bits = comparison_counter_expm_bits

    enc_modulo_counter_modulo_mult = modulo_counter_modulo_mult
    enc_add_counter_modulo_mult = add_counter_modulo_mult
    enc_shift_counter_modulo_mult = shift_counter_modulo_mult
    enc_comparison_counter_modulo_mult = comparison_counter_modulo_mult
    enc_modulo_counter_modulo_mult_bits = modulo_counter_modulo_mult_bits
    enc_add_counter_modulo_mult_bits = add_counter_modulo_mult_bits
    enc_shift_counter_modulo_mult_bits = shift_counter_modulo_mult_bits
    enc_comparison_counter_modulo_mult_bits = comparison_counter_modulo_mult_bits

    enc_shift_counter_booths = shift_counter_booths
    enc_add_counter_booths = add_counter_booths
    enc_modulo_counter_booths = modulo_counter_booths
    enc_comparison_counter_booths = comparison_counter_booths
    enc_shift_counter_booths_bits = shift_counter_booths_bits
    enc_add_counter_booths_bits = add_counter_booths_bits
    enc_modulo_counter_booths_bits = modulo_counter_booths_bits
    enc_comparison_counter_booths_bits = comparison_counter_booths_bits

    message = rsaDec(cipher)

    # prints timing and operand count results csv style (so they can be copied into excel and turned into graphs)
    print(enc_binary_exp, ", ", enc_no_alg, ", ", enc_modulo_mult, ", ", enc_booths, ", ", \
    dec_binary_exp, ", ", dec_modulo_mult, ", ", \
    enc_binary_exp + dec_binary_exp, ", ", enc_modulo_mult + dec_modulo_mult, ", ", \
    enc_mult_counter_expm, ", ", enc_modulo_counter_expm, ", ", enc_comparison_counter_expm, ", ", enc_mult_counter_expm_bits, ", ", enc_modulo_counter_expm_bits, ", ", enc_comparison_counter_expm_bits, ", ", \
    enc_modulo_counter_modulo_mult, ", ", enc_add_counter_modulo_mult, ", ", enc_shift_counter_modulo_mult, ", ", enc_comparison_counter_modulo_mult, ", ", enc_modulo_counter_modulo_mult_bits, ", ", enc_add_counter_modulo_mult_bits, ", ", enc_shift_counter_modulo_mult_bits, ", ", enc_comparison_counter_modulo_mult_bits, ", ", \
    enc_modulo_counter_booths, ", ", enc_add_counter_booths, ", ", enc_shift_counter_booths, ", ", enc_comparison_counter_booths, ", ", enc_modulo_counter_booths_bits, ", ", enc_add_counter_booths_bits, ", ", enc_shift_counter_booths_bits, ", ", enc_comparison_counter_booths_bits, ", ", \
    exponent_counter_no_algorithm_bits, ", ", modulo_counter_no_algorithm_bits, ", ", \
    mult_counter_expm, ", ", modulo_counter_expm, ", ", comparison_counter_expm, ", ", mult_counter_expm_bits, ", ", modulo_counter_expm_bits, ", ", comparison_counter_expm_bits, ", ", \
    modulo_counter_modulo_mult, ", ", add_counter_modulo_mult, ", ", shift_counter_modulo_mult, ", ", comparison_counter_modulo_mult, ", ", modulo_counter_modulo_mult_bits, ", ", add_counter_modulo_mult_bits, ", ", shift_counter_modulo_mult_bits, ", ", comparison_counter_modulo_mult_bits)#, ", ", \
    #modulo_counter_booths, ", ", add_counter_booths, ", ", shift_counter_booths, ", ", modulo_counter_booths_bits, ", ", add_counter_booths_bits, ", ", shift_counter_booths_bits) booths only does encryption, decryption too slow
