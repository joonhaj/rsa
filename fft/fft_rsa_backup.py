#! /usr/local/bin/python3.6
from cmath import exp
from math import pi
import argparse

class Calculation:
    def __init__(self, n, k = 1):
        self.k = k
        self.n = n

    def __pow__(self, other):
        if type(other) is int:
            n = Calculation(self.n, self.k * other)
            return n

    def __eq__(self, other):
        if other == 1:
            return abs(self.n) == abs(self.k)

    def __mul__(self, other):
        return exp(2*1j*pi*self.k/self.n)*other

    def __repr__(self):
        return str(self.n) +  str(self.k)

    @property
    def th(self):
        return abs(self.n // self.k)


def FFT(A, omega):
    if omega == 1:
        return [sum(A)]
    o2 = omega**2
    C1 = FFT(A[0::2], o2)
    C2 = FFT(A[1::2], o2)
    C3 = [None]*omega.th
    for i in range(omega.th//2):
        C3[i] = C1[i] + omega**i * C2[i]
        C3[i+omega.th//2] = C1[i] - omega**i * C2[i]
    return C3

def FPM(A,B):
    n = 1<<(len(A)+len(B)-2).bit_length()
    o = Calculation(n)
    AT = FFT(A, o)
    BT = FFT(B, o)
    C = [AT[i]*BT[i] for i in range(n)]
    D = [round((a/n).real) for a in FFT(C, o ** -1)]
    while len(D) > 0 and D[-1] == 0:
        del D[-1]
    return D

def power(a,b):
    result = 1
    while b > 0:
        if b % 2 != 0:
            result = result * a
        b >>= 1
        a = a * a

    return result

# there is a way to improve the performance
def multiplication(a,b):
    # input value should be integer array
    # input_m = [i for i in map(int, str(input_a))] set message 12345
    # value = FPM([1,2,3,4,5], [1,2,3,4,5]) # FPM(input_m, input_m)
    input_a = [i for i in map(int, str(a))] # set message 12345
    input_b = [i for i in map(int, str(b))] # set message 12345
    
    value = FPM(input_a, input_b)
    
    total = 0 #sum
    for i in range(len(value)):
        total += value[i] * power(10, len(value)-i-1)
    return total

# fft exponentiaion multiplication with modulus
"""
def fft_calculation(a,b,m): 
    # a:message, b:e_value, m:modulus
    result = 1
    while b > 0:
        if b % 2 != 0:
            #result = (result * a) % m
            result = multiplication(result, a) % m
        #b //= 2
        b >>= 1
        #a = (a * a) % m
        a = multiplication(a, a) % m
    return result
"""

def fft_calculation(a, n, m): # m(message), e(65537), n(modulo)
    #global shift_counter_karatsuba, modulo_counter_karatsuba
    r = 1
    while n > 0: #65537
        if n & 1:
            r = multiplication(r, a) % m
        a = multiplication(a, a) % m
        n >>= 1
    return r

""" 
# test to see if fft correctly performs multiplication, this worked
result = multiplication(123456789876543214566525343545869997, 173625475896958763542113243474859697867564)
correct_result = 123456789876543214566525343545869997 * 173625475896958763542113243474859697867564
if correct_result == result:
    print("correct! ", correct_result, " = ", result)
else:
    print("incorrect! ", correct_result, " = ", result)

#print(result)
"""

parser = argparse.ArgumentParser(description='\nRSA_fft\n')
parser.add_argument('--message')
parser.add_argument('--e_value')
parser.add_argument('--modulus')
args=parser.parse_args() 

args.message = int(args.message)
args.e_value = int(args.e_value,16)
args.modulus = int(args.modulus,16)

#result = fft_calculation(args.message, args.e_value, args.modulus)
#print(result)

encryption_result = fft_calculation(12345, 333, 1207) #65537
#encryption_result = fft_calculation(args.message, 65537, args.modulus) #65537
#print("encryption_result = ", encryption_result)
correct_result = 12345**333 % 1207

if correct_result == encryption_result:
    print("correct! ", correct_result, " = ", encryption_result)
else:
    print("incorrect! ", correct_result, " = ", encryption_result, "\n")
    print("correct_result = ", correct_result, "\n")
    print("encryption_result = ", encryption_result, "\n")


"""
decryption_result = fft_calculation(encryption_result, args.e_value, args.modulus)

if decryption_result == args.message:
    print("correct! decryption result = original message, ", decryption_result, " = ", args.message)
else:
    print("incorrect! decryption result != original message, ", decryption_result, " != ", args.message)

"""




