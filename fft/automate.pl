#! /usr/bin/perl

# for loop 

# $mult_sum = 0;
# $modulo_sum = 0;

# $mult_result = 0;
# $modulo_result = 0;

$loop = 1;

`rm example.key`;
`rm rsa_output.txt`;
`touch rsa_output.txt`;
#`python2 print_csv_headers.py >> rsa_output.txt`;

`rm rsa_output_fft.txt`;
`touch rsa_output_fft.txt`;
#`python2 print_csv_headers_fft.py >> rsa_output_fft.txt`;

for($a = 0; $a < $loop; $a = $a + 1){
    # create new key
    `openssl genrsa -out example.key`;
    $modulus_long = `openssl rsa -noout -modulus -in example.key`; #extract modulus
    #print $modulus_long
    $modulus = substr($modulus_long, 8); #remove "modulus="
    $modulus =~ tr/\n\t //d; # remove new lines, tabs, and spaces
    #print "\nmodulus = ";
    #print $modulus;

    $private_key_long = `openssl pkey -in example.key -text -noout`;
    $private_key = substr($private_key_long, 960, 855); #extract private key
    $private_key =~ tr/:\n\tpr //d; #remove colons, new lines, tabs, and spaces (and pr if we overflow to "prime1")
    #print "\nprivate key = ";
    #print $private_key;
    #print "\n";
    # run python code args = modulus (n), message (m), private key (d)
    $message = 12345;
    #`python3 rsa.py $modulus $message $private_key >> rsa_output.txt`;
    `python3 fft_rsa_cmp.py --message $message --e_value $private_key --modulus $modulus >> rsa_output.txt`;
    #`python3 fft_rsa.py --message $message --e_value 66215 --modulus $modulus >> rsa_output.txt`;
    #`python2 rsa_karatsuba.py $modulus $message >> rsa_output_karatsuba.txt`;

    # extract number from mult_counter
    # $str = `grep "mult_counter" rsa_output.txt`;
    # ($mult_num) = $str =~ /(\d+)/;

    # # extract number from modulo_counter
    # $mod_str = `grep "modulo_counter" rsa_output.txt`;
    # ($modulo_num) = $mod_str =~ /(\d+)/;

    # # sum
    # $mult_sum = $mult_sum + $mult_num;
    # $modulo_sum = $modulo_sum + $modulo_num;

}

# average
# $mult_result = $mult_sum / $loop;
# $modulo_result = $mult_sum / $loop;

# print "mult_result = ";
# print $mult_result;
# print "\nmodulo_result = ";
# print $modulo_result;
# print "\n";
