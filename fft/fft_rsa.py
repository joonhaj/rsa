#! /usr/local/bin/python3.6
from cmath import exp
from math import pi
import argparse
import numpy as np

class Calculation:
    def __init__(self, n, k = 1):
        self.k = k
        self.n = n

    def __pow__(self, other):
        if type(other) is int:
            n = Calculation(self.n, self.k * other)
            return n

    def __eq__(self, other):
        if other == 1:
            return abs(self.n) == abs(self.k)

    def __mul__(self, other):
        return exp(2*1j*pi*self.k/self.n)*other

    def __repr__(self):
        return str(self.n) +  str(self.k)

    @property
    def th(self):
        return abs(self.n // self.k)


def FFT(A, omega):
    if omega == 1:
        return [sum(A)]
    o2 = omega**2
    C1 = FFT(A[0::2], o2)
    C2 = FFT(A[1::2], o2)
    C3 = [None]*omega.th
    for i in range(omega.th//2):
        C3[i] = C1[i] + omega**i * C2[i]
        C3[i+omega.th//2] = C1[i] - omega**i * C2[i]
    return C3

def FPM(A,B):
    n = 1<<(len(A)+len(B)-2).bit_length()
    o = Calculation(n)
    AT = FFT(A, o)
    BT = FFT(B, o)
    C = [AT[i]*BT[i] for i in range(n)]
    #print("C : ", C, " n : ", n)
    #D = [round((a/n).real) for a in FFT(C, o ** -1)]
    D = [round((a/n).real) for a in FFT(C, o ** -1)]
    #print("D : ", D)
    #while len(D) > 0 and D[-1] == 0:
    #    del D[-1]
    while (len(A) + len(B) -1) < len(D):
        del D[-1]
    #print("after D: ", D)
    return D

def power(a,b): # not used
    result = 1
    while b > 0:
        if b % 2 != 0:
            result = result * a
        b >>= 1
        a = a * a

    return result

# there is a way to improve the performance
def multiplication(a,b):
    # input value should be integer array
    # input_m = [i for i in map(int, str(input_a))] set message 12345
    # value = FPM([1,2,3,4,5], [1,2,3,4,5]) # FPM(input_m, input_m)
    input_a = [i for i in map(int, str(a))] # set message 12345
    input_b = [i for i in map(int, str(b))] # set message 12345
    
    #print(input_a, input_b)

    value = FPM(input_a, input_b)
    #print("value : ", value)
    
    total = 0 #sum
    for i in range(len(value)):
        total += value[i] * power(10, len(value)-i-1)

    return total
    #return np.polyval(value,10)

# fft exponentiaion multiplication with modulus
"""
def fft_acalculation(a,b,m): 
    # a:message, b:e_value, m:modulus
    result = 1
    while b > 0:
        if b % 2 != 0: #odd
            #result = (result * a) % m
            result = multiplication(result, a)# % m
            print(result)
        b //= 2
        #b >>= 1
        #a = (a * a) % m
        a = multiplication(a, a)# % m
        print(a)
    return result
"""

def fft_calculation(a, n, m): # m(message), e(65537), n(modulo)
    #global shift_counter_karatsuba, modulo_counter_karatsuba
    r = 1
    while n > 0: #65537
        #print("step")
        #print("n : ", n)
        #print("before r: ", r, " a: ", a, " m :", m)
        if n & 1:
            #print(">> r: ", r, " a: ", a, " m :", m)
            #print(">> interm value ", multiplication(r, a))
            r = multiplication(r, a) % m
            #print("r: ",r)
        a = multiplication(a, a) % m
        #print("a: ",a)
        n >>= 1
    return r

# test to see if fft correctly performs multiplication, this worked


#result = multiplication(123, 123) #497 62     11 20
#print("result : ", result)

parser = argparse.ArgumentParser(description='\nRSA_fft\n')
parser.add_argument('--message')
parser.add_argument('--e_value')
parser.add_argument('--modulus')
args=parser.parse_args() 

args.message = int(args.message)
args.e_value = int(args.e_value,16)
args.modulus = int(args.modulus,16)

#result = fft_calculation(args.message, args.e_value, args.modulus)
#print(result)

#encryption_result = fft_calculation(12345, 333, 1207) #65537

#modulus = int("B5D787BE88C7BCD0957275127C4CC385ABD794FAF265A2C92D21F502F5CB062E41E354314678DEB710D41ED8705BEF5E79045EB64924771711E56BCD3339887D697E2F25FF8037F8B8CB1F07FC097C9DA88BA5A7F2550A6D5ABB9C911BE65616FF59943E0FDD5ADB5AFC5802C1807248D37AE19A67AA1E1F8CF5D008D8164E65288594CEF32FBD551ADF63310681C4C241AAA0A258E98AC5DA16E050F3D87CC0DC540B22CE5CCA41AB49E994A3852EBC563FFE61302097FF3811E665A20AECE487997EE234531356B3E4C5431D0955A1A72064E9C23B20377D1E7BA76237996B6DD5D0B96BBDE8DFD2110263D7C86707E3C9588F7DED6185E227B7C74523F343",16)

#encryption_result = fft_calculation(12345, 65537, modulus) #65537
encryption_result = fft_calculation(args.message, 65537, args.modulus) #65537
##print("encryption_result = ", encryption_result)
correct_result = 12345**65537 % args.modulus

if correct_result == encryption_result:
    print("correct! ", correct_result, " = ", encryption_result)
else:
    print("incorrect! ", correct_result, " = ", encryption_result)


decryption_result = fft_calculation(encryption_result, args.e_value, args.modulus)

if decryption_result == args.message:
    print("correct! decryption result = original message, ", decryption_result, " = ", args.message)
else:
    print("incorrect! decryption result != original message, ", decryption_result, " != ", args.message)





